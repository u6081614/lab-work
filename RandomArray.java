/**
 * Created by Tom on 1/03/2017.
 */

import java.util.ArrayList;
import java.util.Random;

public class RandomArray {
    final static int NUM_RANDS = 20;

    public static void main(String[] args) {
        Random randomGenerator = new Random();
        ArrayList<Integer> testList = new ArrayList();
        for(int i = 0; i<NUM_RANDS;i++) {

            int randomInt = randomGenerator.nextInt(100);
            testList.add(randomInt);

        }

        System.out.println(findMax(testList));
        System.out.println(findMin(testList));

        //never runs slow but stack explodes around 100000
        System.out.println(Fib(10000));


    }

    private static int findMax(ArrayList arr) {
        int max = 0;
        for(int i=0; i<arr.size();i++)
            if((Integer) arr.get(i)>max) max = (int) arr.get(i);
        return max;
    }

    private static int findMin(ArrayList arr) {
        int min = (int) arr.get(0);
        for(int i=0; i<arr.size();i++)
            if((Integer) arr.get(i)<min) min = (int) arr.get(i);
        return min;
    }



    private static int Fib(int n) {
        return FF(n,0,1);
    }

    private static int FF(int n, int a, int b) {
        if(n==0) return a;
        return FF(n-1,b,a+b);
    }

    //take a picture in any format and convert it into an ascii image using ascii characters for brightness and darkness
    //first part is to use UML
    //use space for light,


}
